package coding.mybatis.mapper;

import static coding.mybatis.mapper.UsersDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static org.assertj.core.api.Assertions.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isIn;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import coding.mybatis.config.MyBatisApplication;
import coding.mybatis.mapper.Users;
import coding.mybatis.mapper.UsersMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = MyBatisApplication.class)
public class UserMapperTest {

	@Autowired
	UsersMapper userMapper;
	private static String userId;

	@Test
	public void testAddUser() {
		Users user = new Users();
		userId = UUID.randomUUID().toString();
		user.setId(userId);
		user.setCreatedTime(new Date());
		user.setFirstName("Rohit");
		user.setLastName("Tiwari");
		user.setPreferredLanguage("English");
		user.setPicture("image.jpg");
		assertThat(userMapper.insert(user)).isEqualTo(1);
	}

	@Test
	public void testUserById() {
		SelectStatementProvider selectStatement = select(id, firstName, lastName, preferredLanguage, createdTime,
				lastUpdatedTime, picture).from(users).where(id, isIn(userId)).orderBy(firstName).build()
						.render(RenderingStrategy.MYBATIS3);
		Optional<Users> user = userMapper.selectOne(selectStatement);
		assertThat(user.get().getId()).isEqualTo(userId);
		assertThat(user.get().getFirstName()).isEqualTo("Rahul");
	}

	@Test
	public void testUserList() {
		SelectStatementProvider selectStatement = select(id, firstName, lastName, preferredLanguage).from(users)
				.where(id, isNotNull()).orderBy(firstName).build().render(RenderingStrategy.MYBATIS3);
		assertThat(userMapper.selectMany(selectStatement).size()).isGreaterThanOrEqualTo(1);
	}

	@Test
	public void testUpdateUser() {
		UpdateStatementProvider updateStatement = update(users).set(firstName).equalToWhenPresent("Rahul").set(lastName)
				.equalToWhenPresent("Tiwari").set(preferredLanguage).equalToWhenPresent("Hindi")
				.where(id, isIn(userId)).build().render(RenderingStrategy.MYBATIS3);

		assertThat(userMapper.update(updateStatement)).isEqualTo(1);
		SelectStatementProvider selectStatement = select(id, firstName, lastName, preferredLanguage, createdTime,
				lastUpdatedTime, picture).from(users).where(id, isIn(userId)).build()
						.render(RenderingStrategy.MYBATIS3);
		Optional<Users> user = userMapper.selectOne(selectStatement);
		assertThat(user.get().getFirstName()).isEqualTo("Rahul");
	}
	@Test
	public void testDeleteUser() {
		DeleteStatementProvider deleteStatement = deleteFrom(users)
                .where(id, isIn(userId))
                .build()
                .render(RenderingStrategy.MYBATIS3);
		assertThat(userMapper.delete(deleteStatement)).isEqualTo(1);
	}

}