package coding.mybatis.mapper;

import java.util.Date;
import javax.annotation.Generated;

public class Users {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.314+05:30", comments="Source field: users.id")
    private String id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.323+05:30", comments="Source field: users.created_by")
    private String createdBy;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.324+05:30", comments="Source field: users.created_time")
    private Date createdTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.last_updated_by")
    private String lastUpdatedBy;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.last_updated_time")
    private Date lastUpdatedTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.first_name")
    private String firstName;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.last_name")
    private String lastName;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.picture")
    private String picture;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.preferred_language")
    private String preferredLanguage;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.331+05:30", comments="Source field: users.background_check")
    private Integer backgroundCheck;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.322+05:30", comments="Source field: users.id")
    public String getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.323+05:30", comments="Source field: users.id")
    public void setId(String id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.324+05:30", comments="Source field: users.created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.324+05:30", comments="Source field: users.created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.created_time")
    public Date getCreatedTime() {
        return createdTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.created_time")
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.last_updated_by")
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.last_updated_by")
    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.last_updated_time")
    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.329+05:30", comments="Source field: users.last_updated_time")
    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.first_name")
    public String getFirstName() {
        return firstName;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.last_name")
    public String getLastName() {
        return lastName;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.picture")
    public String getPicture() {
        return picture;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.33+05:30", comments="Source field: users.picture")
    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.331+05:30", comments="Source field: users.preferred_language")
    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.331+05:30", comments="Source field: users.preferred_language")
    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.331+05:30", comments="Source field: users.background_check")
    public Integer getBackgroundCheck() {
        return backgroundCheck;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.331+05:30", comments="Source field: users.background_check")
    public void setBackgroundCheck(Integer backgroundCheck) {
        this.backgroundCheck = backgroundCheck;
    }
}