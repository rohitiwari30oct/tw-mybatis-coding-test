package coding.mybatis.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class UsersDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.345+05:30", comments="Source Table: users")
    public static final Users users = new Users();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.346+05:30", comments="Source field: users.id")
    public static final SqlColumn<String> id = users.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.347+05:30", comments="Source field: users.created_by")
    public static final SqlColumn<String> createdBy = users.createdBy;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.347+05:30", comments="Source field: users.created_time")
    public static final SqlColumn<Date> createdTime = users.createdTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.348+05:30", comments="Source field: users.last_updated_by")
    public static final SqlColumn<String> lastUpdatedBy = users.lastUpdatedBy;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.348+05:30", comments="Source field: users.last_updated_time")
    public static final SqlColumn<Date> lastUpdatedTime = users.lastUpdatedTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.348+05:30", comments="Source field: users.first_name")
    public static final SqlColumn<String> firstName = users.firstName;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.348+05:30", comments="Source field: users.last_name")
    public static final SqlColumn<String> lastName = users.lastName;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.348+05:30", comments="Source field: users.picture")
    public static final SqlColumn<String> picture = users.picture;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.349+05:30", comments="Source field: users.preferred_language")
    public static final SqlColumn<String> preferredLanguage = users.preferredLanguage;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.349+05:30", comments="Source field: users.background_check")
    public static final SqlColumn<Integer> backgroundCheck = users.backgroundCheck;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.346+05:30", comments="Source Table: users")
    public static final class Users extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> createdBy = column("created_by", JDBCType.VARCHAR);

        public final SqlColumn<Date> createdTime = column("created_time", JDBCType.TIMESTAMP);

        public final SqlColumn<String> lastUpdatedBy = column("last_updated_by", JDBCType.VARCHAR);

        public final SqlColumn<Date> lastUpdatedTime = column("last_updated_time", JDBCType.TIMESTAMP);

        public final SqlColumn<String> firstName = column("first_name", JDBCType.VARCHAR);

        public final SqlColumn<String> lastName = column("last_name", JDBCType.VARCHAR);

        public final SqlColumn<String> picture = column("picture", JDBCType.VARCHAR);

        public final SqlColumn<String> preferredLanguage = column("preferred_language", JDBCType.VARCHAR);

        public final SqlColumn<Integer> backgroundCheck = column("background_check", JDBCType.INTEGER);

        public Users() {
            super("users");
        }
    }
}