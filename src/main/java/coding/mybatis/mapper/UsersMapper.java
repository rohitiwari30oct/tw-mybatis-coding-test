package coding.mybatis.mapper;

import static coding.mybatis.mapper.UsersDynamicSqlSupport.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

import coding.mybatis.mapper.Users;

@Mapper
public interface UsersMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.376+05:30", comments="Source Table: users")
    BasicColumn[] selectList = BasicColumn.columnList(id, createdBy, createdTime, lastUpdatedBy, lastUpdatedTime, firstName, lastName, picture, preferredLanguage, backgroundCheck);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.35+05:30", comments="Source Table: users")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.354+05:30", comments="Source Table: users")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.355+05:30", comments="Source Table: users")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<Users> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.357+05:30", comments="Source Table: users")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<Users> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.358+05:30", comments="Source Table: users")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("UsersResult")
    Optional<Users> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.359+05:30", comments="Source Table: users")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="UsersResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="created_by", property="createdBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="created_time", property="createdTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="last_updated_by", property="lastUpdatedBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="last_updated_time", property="lastUpdatedTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="picture", property="picture", jdbcType=JdbcType.VARCHAR),
        @Result(column="preferred_language", property="preferredLanguage", jdbcType=JdbcType.VARCHAR),
        @Result(column="background_check", property="backgroundCheck", jdbcType=JdbcType.INTEGER)
    })
    List<Users> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.365+05:30", comments="Source Table: users")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.366+05:30", comments="Source Table: users")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, users, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.367+05:30", comments="Source Table: users")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, users, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.368+05:30", comments="Source Table: users")
    default int insert(Users record) {
        return MyBatis3Utils.insert(this::insert, record, users, c ->
            c.map(id).toProperty("id")
            .map(createdBy).toProperty("createdBy")
            .map(createdTime).toProperty("createdTime")
            .map(lastUpdatedBy).toProperty("lastUpdatedBy")
            .map(lastUpdatedTime).toProperty("lastUpdatedTime")
            .map(firstName).toProperty("firstName")
            .map(lastName).toProperty("lastName")
            .map(picture).toProperty("picture")
            .map(preferredLanguage).toProperty("preferredLanguage")
            .map(backgroundCheck).toProperty("backgroundCheck")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.37+05:30", comments="Source Table: users")
    default int insertMultiple(Collection<Users> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, users, c ->
            c.map(id).toProperty("id")
            .map(createdBy).toProperty("createdBy")
            .map(createdTime).toProperty("createdTime")
            .map(lastUpdatedBy).toProperty("lastUpdatedBy")
            .map(lastUpdatedTime).toProperty("lastUpdatedTime")
            .map(firstName).toProperty("firstName")
            .map(lastName).toProperty("lastName")
            .map(picture).toProperty("picture")
            .map(preferredLanguage).toProperty("preferredLanguage")
            .map(backgroundCheck).toProperty("backgroundCheck")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.372+05:30", comments="Source Table: users")
    default int insertSelective(Users record) {
        return MyBatis3Utils.insert(this::insert, record, users, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(createdBy).toPropertyWhenPresent("createdBy", record::getCreatedBy)
            .map(createdTime).toPropertyWhenPresent("createdTime", record::getCreatedTime)
            .map(lastUpdatedBy).toPropertyWhenPresent("lastUpdatedBy", record::getLastUpdatedBy)
            .map(lastUpdatedTime).toPropertyWhenPresent("lastUpdatedTime", record::getLastUpdatedTime)
            .map(firstName).toPropertyWhenPresent("firstName", record::getFirstName)
            .map(lastName).toPropertyWhenPresent("lastName", record::getLastName)
            .map(picture).toPropertyWhenPresent("picture", record::getPicture)
            .map(preferredLanguage).toPropertyWhenPresent("preferredLanguage", record::getPreferredLanguage)
            .map(backgroundCheck).toPropertyWhenPresent("backgroundCheck", record::getBackgroundCheck)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.38+05:30", comments="Source Table: users")
    default Optional<Users> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, users, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.38+05:30", comments="Source Table: users")
    default List<Users> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, users, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.381+05:30", comments="Source Table: users")
    default List<Users> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, users, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.382+05:30", comments="Source Table: users")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, users, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.383+05:30", comments="Source Table: users")
    static UpdateDSL<UpdateModel> updateAllColumns(Users record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(createdBy).equalTo(record::getCreatedBy)
                .set(createdTime).equalTo(record::getCreatedTime)
                .set(lastUpdatedBy).equalTo(record::getLastUpdatedBy)
                .set(lastUpdatedTime).equalTo(record::getLastUpdatedTime)
                .set(firstName).equalTo(record::getFirstName)
                .set(lastName).equalTo(record::getLastName)
                .set(picture).equalTo(record::getPicture)
                .set(preferredLanguage).equalTo(record::getPreferredLanguage)
                .set(backgroundCheck).equalTo(record::getBackgroundCheck);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-06-04T17:24:11.384+05:30", comments="Source Table: users")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(Users record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(createdBy).equalToWhenPresent(record::getCreatedBy)
                .set(createdTime).equalToWhenPresent(record::getCreatedTime)
                .set(lastUpdatedBy).equalToWhenPresent(record::getLastUpdatedBy)
                .set(lastUpdatedTime).equalToWhenPresent(record::getLastUpdatedTime)
                .set(firstName).equalToWhenPresent(record::getFirstName)
                .set(lastName).equalToWhenPresent(record::getLastName)
                .set(picture).equalToWhenPresent(record::getPicture)
                .set(preferredLanguage).equalToWhenPresent(record::getPreferredLanguage)
                .set(backgroundCheck).equalToWhenPresent(record::getBackgroundCheck);
    }
}