package coding.mybatis.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import static org.mybatis.dynamic.sql.SqlBuilder.isNotNull;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static coding.mybatis.mapper.UsersDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import com.fasterxml.jackson.core.JsonProcessingException;

import coding.mybatis.exception.CustomException;
import coding.mybatis.util.Utility;

@RestController
public class UserController {
	@Autowired
	UsersMapper usersMapper;

	@GetMapping(value = "/users", produces = APPLICATION_JSON_UTF8_VALUE)
	List<Users> getUsersd() {
		SelectStatementProvider selectStatement = select(id, firstName, lastName, preferredLanguage).from(users)
				.where(id, isNotNull()).orderBy(firstName).build().render(RenderingStrategy.MYBATIS3);
		return usersMapper.selectMany(selectStatement);
	}

	@GetMapping(value = "/user/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
	Users getUserById(@PathVariable(name = "id") String userId) {
		SelectStatementProvider selectStatement = select(id, firstName, lastName, preferredLanguage, createdTime,
				lastUpdatedTime, picture).from(users).where(id, isIn(userId)).orderBy(firstName).build()
						.render(RenderingStrategy.MYBATIS3);
		Optional<Users> user = usersMapper.selectOne(selectStatement);
		if (user.isPresent())
			return user.get();
		throw new CustomException("Error occurred, no record found");
	}

	@PostMapping(value = "/user/add", produces = APPLICATION_JSON_UTF8_VALUE)
	String addNewUser(@RequestBody Users user) throws JsonProcessingException {
		user.setId(UUID.randomUUID().toString());
		user.setCreatedTime(new Date());
		if (usersMapper.insert(user) > 0) {
			return Utility.getSuccessResponseJson("Saved successfully");
		}
		throw new CustomException("Error occurred, please try again");
	}

	@PostMapping(value = "/user/update", produces = APPLICATION_JSON_UTF8_VALUE)
	String updateUser(@RequestBody Users user) throws JsonProcessingException {
		UpdateStatementProvider updateStatement = update(users).set(firstName).equalToWhenPresent(user.getFirstName())
				.set(lastName).equalToWhenPresent(user.getLastName()).set(preferredLanguage)
				.equalToWhenPresent(user.getPreferredLanguage()).where(id, isIn(user.getId())).build()
				.render(RenderingStrategy.MYBATIS3);
		if (usersMapper.update(updateStatement) > 0) {
			return Utility.getSuccessResponseJson("Saved successfully");
		}
		throw new CustomException("Error occurred, please try again");
	}

	@PostMapping(value = "/user/delete", produces = APPLICATION_JSON_UTF8_VALUE)
	String deleteUser(@RequestBody Users user) throws JsonProcessingException {
		DeleteStatementProvider deleteStatement = deleteFrom(users).where(id, isIn(user.getId())).build()
				.render(RenderingStrategy.MYBATIS3);
		if (usersMapper.delete(deleteStatement) > 0) {
			return Utility.getSuccessResponseJson("Deleted successfully");
		}
		throw new CustomException("Error occurred, no record available");
	}

}
