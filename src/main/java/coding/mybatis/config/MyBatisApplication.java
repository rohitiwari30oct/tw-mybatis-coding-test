package coding.mybatis.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="coding.mybatis")
@EnableAutoConfiguration
@MapperScan("coding.mybatis.mapper")
public class MyBatisApplication {
	    public static void main(String[] args)
	    {
	        SpringApplication.run(MyBatisApplication.class, args);
	    }
}
