package coding.mybatis.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utility {
	/**
	 * @param value
	 * @return true is value is neither empty nor null
	 */
	public static boolean checkNullEmpty(String value) {
		if(value!=null&&!value.trim().equals("")&&!value.equals("null")) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * @param message
	 * @return generate common success response
	 * @throws JsonProcessingException
	 */
	public static String getSuccessResponseJson(String message) throws JsonProcessingException {
        Map<String, String> successRes = new HashMap<>();
        successRes.put("status", HttpStatus.OK.toString());
        successRes.put("message", message);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(successRes);
    }
}
