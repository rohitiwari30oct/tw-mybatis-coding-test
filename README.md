# tw-mybatis-coding-test

This is a coding test for tw backend developer candidates.

#Objective
Use [Mybatis Mapper](http://www.mybatis.org/mybatis-3) to write a basic CRUD application on a User table(users.sql) against a postgres database.

# Instructions to code
*  This repo is readonly, so fork this repo on gitlab and create new repo with public access
*  use spring-boot and maven to give structure to the application
*  use mybatis code-generator to auto-generate the mapping code
*  use junit to write a basic test suite for the CRUD operations
*  update the readme to add accurate instructions on how to setup and run the project and run the tests
*  Share the forked repo link with the hr

# Instructions to run 

*  Clone the project and configure as Maven project
*  replace demo database values in application.properties file
*  to run all test cases, execute : mvn -Dtest=UserMapperTest test
*  to package application into jar file, execute : mvn clean package 

# or 

*  run as SpringBoot Application from IDE

# Below are application api urls : 

*  Add new user : /user/add

*  Example :

*  request : 
{
    "firstName" : "",
    "lastName" : "",
    "preferredLanguage" : ""
}

*  success response : 
{
    "message": "Saved successfully",
    "status": "200 OK"
}

*  Update User : /user/update

*  Example : 

*  request
{
	"id" : "",
	"firstName" : "",
	"lastName" : ""
}


*  success response : 
{
    "message": "Saved successfully",
    "status": "200 OK"
}

*  delete user : /user/delete

*  request 
{
	"id" : ""
}
*  success response : 
{
    "message": "Deleted successfully",
    "status": "200 OK"
}

*  List all users : /users
response : 
[
    {
        "id": "",
        "createdBy": null,
        "createdTime": null,
        "lastUpdatedBy": null,
        "lastUpdatedTime": null,
        "firstName": "",
        "lastName": "",
        "picture": null,
        "preferredLanguage": "",
        "backgroundCheck": null
    },...
    ...
    
*  get individual user : /user/{userid}
response
{
        "id": "",
        "createdBy": null,
        "createdTime": null,
        "lastUpdatedBy": null,
        "lastUpdatedTime": null,
        "firstName": "",
        "lastName": "",
        "picture": null,
        "preferredLanguage": "",
        "backgroundCheck": null
}

Haven't use service layer because of less time. 